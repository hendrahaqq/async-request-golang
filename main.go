package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
)

func main() {
	// urls := []string{
	// 	"https://www.sekolahdesain.id/",
	// 	"https://soizee.com/",
	// 	"https://refactory.id/",
	// 	"https://enroll.refactory.id/",
	// 	"https://www.facebook.com/",
	// 	"https://www.instagram.com/",
	// 	"https://www.google.com/",
	// 	"https://www.bing.com/",
	// 	"https://data.go.id/",
	// }

	// for _, url := range urls {
	// 	go checkUrl(url)
	// }
	// time.Sleep(1 * time.Second)

	// var wg sync.WaitGroup

	// for _, u := range urls {
	// 	wg.Add(1)
	// 	go func(url string) {
	// 		defer wg.Done()

	// 		checkUrl(url)
	// 	}(u)
	// }
	// wg.Wait()
	var wg sync.WaitGroup
	wg.Add(5)

	go getOneGame(&wg)
	go getOneTeam(&wg)
	go getOnePlayer(&wg)
	go weaponGet(&wg)
	go foodList(&wg)

	// go dataGet()
	// go nbaGet()
	// go dataPostFormData()
	// go dataPost()
	// go dataGet()

	wg.Wait()
}
func getOneGame(wg *sync.WaitGroup) {
	defer wg.Done()
	url := "https://rapidapi.p.rapidapi.com/games/1"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "free-nba.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "5e183d1407msh1eb847068830fbbp15e5dfjsn7e308a51e079")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("==>One Game: ", string(body))
}
func getOnePlayer(wg *sync.WaitGroup) {
	defer wg.Done()
	url := "https://rapidapi.p.rapidapi.com/players/3"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "free-nba.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "5e183d1407msh1eb847068830fbbp15e5dfjsn7e308a51e079")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("==>One Player: ", string(body))
}
func foodList(wg *sync.WaitGroup) {
	defer wg.Done()
	url := "https://rapidapi.p.rapidapi.com/parser?ingr=apple"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "edamam-food-and-grocery-database.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "5e183d1407msh1eb847068830fbbp15e5dfjsn7e308a51e079")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("==>Food: ", string(body))
}
func weaponGet(wg *sync.WaitGroup) {
	defer wg.Done()

	url := "https://rapidapi.p.rapidapi.com/statistics"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "covid-193.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "5e183d1407msh1eb847068830fbbp15e5dfjsn7e308a51e079")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("==>Weapon: ", string(body))
}
func getOneTeam(wg *sync.WaitGroup) {
	defer wg.Done()

	url := "https://rapidapi.p.rapidapi.com/teams/2"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "free-nba.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "5e183d1407msh1eb847068830fbbp15e5dfjsn7e308a51e079")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("==>One Team: ", string(body))
}

func checkUrl(url string) {
	_, err := http.Get(url)
	if err != nil {
		fmt.Println(url, "is down !!")
		return
	}
	fmt.Println(url, "is up and running.")
}
func dataGet(wg *sync.WaitGroup) {
	defer wg.Done()
	resp, err1 := http.Get("https://httpbin.org/get")
	if err1 != nil {
		fmt.Println(err1)
	}
	body, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		fmt.Println(err2)
	}
	fmt.Println("ini dataget: ", string(body))
}

func dataPost(wg *sync.WaitGroup) {

	defer wg.Done()
	reQuestBody, err1 := json.Marshal(map[string]string{
		"name":  "bada",
		"email": "bada@gmail.com",
	})
	if err1 != nil {
		fmt.Println(err1)
	}
	resp, err2 := http.Post("https://httpbin.org/post", "application/json", bytes.NewBuffer(reQuestBody))
	if err2 != nil {
		fmt.Println(err2)
	}

	body, err3 := ioutil.ReadAll(resp.Body)
	if err3 != nil {
		fmt.Println(err3)
	}
	fmt.Println("ini dataPost: ", string(body))
}

func dataPostFormData(wg *sync.WaitGroup) {
	defer wg.Done()
	formData := url.Values{
		"name": {"lili"},
	}

	resp, err2 := http.PostForm("https://httpbin.org/post", formData)
	if err2 != nil {
		fmt.Println(err2)
	}

	var result map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&result)

	fmt.Println("ini datapostformdata: ", result["form"])
}
